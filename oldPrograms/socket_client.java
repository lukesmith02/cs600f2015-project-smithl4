//java socket client example
import java.io.*;
import java.util.*;
import java.net.*;
 
public class socket_client
{
    public static void main(String[] args) throws IOException 
    {
        Socket s = new Socket();
    String host = "aldenv106.allegheny.edu";
    PrintWriter s_out = null;
    BufferedReader s_in = null;
         
        try
        {
        s.connect(new InetSocketAddress(host , 12345));
        System.out.println("Connected");
             
        //writer for socket
            s_out = new PrintWriter( s.getOutputStream(), true);
            //reader for socket
            s_in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        }
         
        //Host not found
        catch (UnknownHostException e) 
        {
            System.err.println("Don't know about host : " + host);
            System.exit(1);
        }
         
        //Send message to server
    Scanner scan = new Scanner(System.in);
    while (true) {
    String message = scan.nextLine();
    s_out.println( message );
             
         
    //Get response from server
    String response;
    while ((response = s_in.readLine()) != null) 
    {
        System.out.println( response );
        System.out.println();
        break;
    }
    }
	}
}
