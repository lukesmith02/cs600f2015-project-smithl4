
To run bash commands in java, you can used the .exec() feature. In order to use
pipes in the bash command, the workaround is to store the command in a string
array.

static String[] cmd = {
		"/bin/sh",
		"-c",
		"ps -aux | awk -F \' \' \'{ x = x + $3 } END { print x }\'"
	};

Process p = Runtime.getRuntime().exec(cmd);

_____________________________________________________________________________

To make multiple threads, you make a class that extends thread for what you
want the thread to do.  This class has to have a constructor and a run method.
The run method is what the thread does. The class can also have additional
methods that you can call. I use a different method for initiating the auction.

____________________________________________________________________________

To set up socket communication, first the server socket must be open to receive client socket requests. Once the server socket is ready, then client sockets can connect to them. 

ServerSocket ss = new ServerSocket(socketNum);

This line creates a new socket on the port number socketNum. 

Socket s= new Socket();
s.connect(new InetSocketAddress(host , socketNum));

These lines create a communication port between the pair.

__________________________________________________________________________________

s_out.println( message );
s_out.flush();

This is how you send messages.
