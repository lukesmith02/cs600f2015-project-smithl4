// echo server
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.net.*;
import java.io.*;
import java.sql.Timestamp;


public class RandomAgent {

	//
	//INITIALIZE VARIABLES
	//
	public static int THRESHOLD=25;
	public static int PTIME=10;
	//time before it ignores process


	public static String[] usedCpu = {
		"/bin/sh",
		"-c",
		"ps -aux | awk -F \' \' \'{ x = x + $3 } END { print x }\'"};
	//bash script used to calculated the CPU
	public static String[] cpuCore = {
		"/bin/sh",
		"-c",
		"cat /proc/cpuinfo | grep cores | head -1"};
	//bash script used to calculated the CPU cores

	static public int machinecount=0; 
	//how many machines are on the network
	public static ArrayList<Double> auction = new ArrayList<Double>();
	//will contain the CPU values for the machines of the network for the auction
	public static Map<Double,String> auctionpair = new HashMap<Double,String>();
	//will contain the machine names and their CPU
	//list of client threads.
	public static int cores=0;


	public static void main(String args[]){
		//
		//INITIALIZE VARIABLES
		//
		String clas="";
		BufferedReader input;
		long start_time=0;
		long end_time=0;
		double difference=-1;
		String s = null;
		//String for filereader
		File file=null;
		Scanner fscan=null;
		//file scanner for machines.txt
		String mname=null;
		//machine name variable
		String process="";
		String CMD="";
		Scanner scan = new Scanner(System.in);
		try {
			Process a = Runtime.getRuntime().exec(cpuCore);
			a.waitFor();
			input = new BufferedReader(new InputStreamReader(a.getInputStream()));
			//output of the exec cmd
			process=input.readLine();
			input.close();
			//close the input we started earlier
		}catch(Exception e){
		}

		cores = Integer.parseInt(process.substring(process.length()-1,process.length()));
		System.out.println(cores);
		csvCreate(mname);
		System.out.println("Created csv");


		try{
			file = new File("../machines2.txt");
			fscan = new Scanner(file);
		}
		catch(Exception e){
		}
		ArrayList<String> machines = new ArrayList<String>();
		while ((fscan.hasNext())){
			s = fscan.nextLine();
			machines.add(s);
			}
		int listsize=machines.size();

			System.out.println(machines);
		while (true){
			//s=scan.nextLine();
			try {
				Process p = Runtime.getRuntime().exec(usedCpu);
				p.waitFor();
				//get the used CPU of the original machine
				input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				//output of the exec cmd
				process=input.readLine();
				input.close();
				//close the input we started earlier
			}catch(Exception e){
			}
			//output of the exec cmd
			double cpu = Double.parseDouble(process);
			cpu=cpu/cores;
			int ptime= Integer.parseInt(processDetect("time"));
			if ((cpu >= 25) && (ptime>=0) && (ptime<=PTIME))  {
				//is above threshold and processDetect() detects something
				start_time = System.nanoTime();
				int id = Integer.parseInt(processDetect("processid"));

				try {
					Process x = Runtime.getRuntime().exec("kill -STOP "+id);
					x.waitFor();
				}catch(Exception e){
				}

				String[] proD =processDetect("java").split(" ");
				String dir = proD[0];
				//gets the directory location of the most recent running java process
				String classs = proD[1];
				//gets the name of the most recent running java process including the .class
				clas = classs.substring(0,classs.length()-6);
				//gets the name of the most recent java program


				try {
					Process y = Runtime.getRuntime().exec("kill -9 "+id);
					y.waitFor();
				}catch(Exception e){
				}
				Random rand = new Random();
				int rs=rand.nextInt(listsize);
				String r=machines.get(rs);
				System.out.println("Debug winner "+r);
				String aldenv=r;
				String[] redirect = {
					"/bin/bash",
					"-c",
					"ssh "+aldenv+" \'java -cp /tmp/Agent "+clas+" >> /tmp/Agent/out"+clas+".txt\'"
				};
				//bash script for remotely running the java process and redirecting the output to a file
				String[] sshremove = {
					"/bin/bash",
					"-c",
					"ssh "+aldenv+" \'rm /tmp/Agent/out+"+clas+".txt\'"
				};
				//removes the file on the remote machine
				String[] remove = {
					"/bin/bash",
					"-c",
					"rm /tmp/Agent/out"+clas+".txt"
				};
				//removes the file on the local machine
				/*
				   System.out.println(aldenv);
				   System.out.println(dir);
				   System.out.println(classs);
				   System.out.println(clas);
				   */
				try{
					Process o = Runtime.getRuntime().exec(remove);
					o.waitFor();
					//run proccess, then sleep 1 second

					Process p = Runtime.getRuntime().exec(sshremove);
					p.waitFor();
					//run proccess, then sleep 1 second

					Process q = Runtime.getRuntime().exec("scp "+dir+"/"+classs+" "+aldenv+":/tmp/Agent/"+classs);
					q.waitFor();
					//run proccess, then sleep 1 second

					CMD= "ssh "+aldenv+" \'java -cp /tmp/Agent "+clas+" >> /tmp/Agent/out"+clas+".txt\'";

					Process aa = Runtime.getRuntime().exec(redirect);
					aa.waitFor();
					//run proccess, then sleep 1 second

					Process t = Runtime.getRuntime().exec("scp "+aldenv+":/tmp/Agent/out"+clas+".txt /tmp/Agent");
					t.waitFor();
					//run proccess, then sleep 1 second
					System.out.println("here");
					//Process u = Runtime.getRuntime().exec("cat /Agent/out"+clas+".txt");
					//u.waitFor();
					//all of these run a command
				}catch (Exception e) {
					System.out.println("crash at process");
				}
				end_time = System.nanoTime();
				System.out.println("Done");
				difference = (end_time - start_time)/1e9;
				//converts to seconds from nano
				csvFinish(clas,mname,CMD,start_time,end_time,difference);
			}
		}
	}

	//start new auction


	public static void csvCreate(String mname){
		String[] csvCreate = {
			"/bin/bash",
			"-c",
			"echo \"TIMESTAMP,JAVA_NAME,MACHINE_NAME,CMD,THRESHOLD,PTIME,START,END,SECDIFFERENCE,NUMMACHINES,TRANSFER,BRDM1,BRDM2,BRDM3,BRDM4,BRDM5,BRDM6,BRDM7,BRDM8,BRDM9,BRDM10,BRDM11,BRDM12,BRDM13,BRDM14,BRDM15,BRDM16,BRDM17,BRDM18,BRDM19,BRDM20\" >> ~/csv_"+mname+".csv"
		};
		try{
			Process u = Runtime.getRuntime().exec(csvCreate);
			u.waitFor();
		}catch(Exception e){
		}
	}


	public static void csvFinish(String javaname, String mname, String CMD, Long start_time, Long end_time, Double difference){
		Date date= new java.util.Date();
		Timestamp TIMESTAMP = new Timestamp(date.getTime());
		String[] csvFinish = {
			"/bin/bash",
			"-c",
			"echo \""+TIMESTAMP+","+javaname+","+mname+","+CMD+","+THRESHOLD+","+PTIME+","+start_time+","+end_time+","+difference+"\" >> ~/csv_"+mname+".csv"
		};
		try{
			Process w = Runtime.getRuntime().exec(csvFinish);
		}catch(Exception e){
		}
	}



	public static String processDetect(String ss){
		String timee="-1";
		String[] processcmd = {
			"/bin/sh",
			"-c",
			"ps -Ao etimes,pid,cmd --sort=etime | grep \"java \""
		};
		String r="-1";
		String processID="-1";
		try {
			String process;
			// getRuntime: Returns the runtime object associated with the current Java application.
			// exec: Executes the specified string command in a separate process.
			Process p = Runtime.getRuntime().exec(processcmd);
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((process = input.readLine()) != null) {
				//System.out.println(process);
				if (!process.contains("/bin/sh")){
					if(!process.contains("grep")){
						if(!process.contains("ssh")){
							if(!process.contains("Agent")){
								if(!process.contains("Process")){
									String[] a = process.split("java ");
									int scount=0;
									String java="java ";
									for (String s : a){
										if (scount==0){
											for (char c : s.toCharArray()){
												if (((c!=' ')) && (timee.equals("-1")))
													timee=c+"";
												else if (!timee.equals("-1"))
													timee=timee+c;
											}
											for (int i=0;i<2;i++){
												if (timee.charAt(timee.length()-1)==' '){
													timee=timee.substring(0,timee.length()-1);
												}
											}
											//System.out.println(timee);
										}
										else
											java=java+s;
										scount++;
									}
									//System.out.println(time);
									//System.out.println(java);
									r=java.split(" ")[2] + " " + java.split(" ")[3] + ".class";

								}
							}
						}
					}
				}
				//System.out.println(process); // <-- Print all Process here line
				// by line	
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		timee=timee.replaceAll("     "," ");
		timee=timee.replaceAll("    "," ");
		timee=timee.replaceAll("   "," ");
		timee=timee.replaceAll("  "," ");
		timee=timee.replaceAll(" "," ");
		if (!timee.contains("-")){
			//System.out.println(timee);
		}
		if (timee.split(" ").length >= 2) {
			processID=timee.split(" ")[1];
			timee=timee.split(" ")[0];
		}
		if (ss.equals("java")){
			return r;
		}
		else if (ss.equals("processid")){
			return processID;
		}
		else if (ss.equals("time")){
			return timee;
		}
		return "error";
	}
}
