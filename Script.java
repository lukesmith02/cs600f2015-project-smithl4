import java.util.*;
import java.net.*;
import java.io.*;
public class Script{
	static int counter=1;
	static int csvCount=-2;

	public static void main(String args[]){
		Random rand = new Random();

		try{
			File file = new File("tests.txt");
			Scanner fscan = new Scanner(file);
			String csvLine="";

			//background/transfer,machinename,job,initial sleep, sleep before kill 
			//(taken as command line parameter from java program)


			while ((fscan.hasNext())){
				csvCount++;
				String s = fscan.nextLine();
				String[] split = s.split(",");
				int splitr=rand.nextInt(4);
				String splitrs=Integer.toString(splitr);
				if (split[0].equals("b")){
					csvLine=csvLine+splitr+",";
					if (splitrs.equals("1")) {
						startBackground(split[1],Integer.parseInt(split[2]),Integer.parseInt(split[3]),"One");
					}
					if (splitrs.equals("2")) {
						startBackground(split[1],Integer.parseInt(split[2]),Integer.parseInt(split[3]),"One");
						startBackground(split[1],Integer.parseInt(split[2]),Integer.parseInt(split[3]),"Two");					
					}
					if (splitrs.equals("3")) {
						startBackground(split[1],Integer.parseInt(split[2]),Integer.parseInt(split[3]),"One");
						startBackground(split[1],Integer.parseInt(split[2]),Integer.parseInt(split[3]),"Two");
						startBackground(split[1],Integer.parseInt(split[2]),Integer.parseInt(split[3]),"Three");
					}
				}
				if (split[0].equals("t")){
					csvLine=split[4]+","+csvLine;
					startTransfer(split[1],Integer.parseInt(split[2]),Integer.parseInt(split[3]),counter);
					counter++;
				}
				if (split[0].equals("x")){
					csvLine=csvCount+","+csvLine;
					while(csvCount<=30){
						csvLine=csvLine+",";
						csvCount++;
					}
					System.out.println(csvLine);
					csvLine="";
					csvCount=-2;
					try{
						Thread.sleep(180000);
					}catch(Exception e){
					}
				}
			}
		}
		catch(Exception e){
		}
	}


	public static void startBackground(String mname,int sleep, int kill,String s){
		BackgroundJob bj = new BackgroundJob(mname,sleep,kill,s);
		bj.start();
	}
	public static void startTransfer(String mname,int sleep, int kill, int counter){
		TransferJob tj = new TransferJob(mname,sleep,kill,counter);
		tj.start();
	}

}


class BackgroundJob extends Thread {
	String mname;
	int sleep;
	int kill;
	String s;

	public BackgroundJob(String a,int b, int c, String s){
		this.mname=a;
		this.sleep=b;
		this.kill=c;
		this.s=s;
	}

	public void run() {
		String[] process={
			"/bin/bash",
			"-c",
			"ssh aldenv"+mname+" \'java -cp /tmp/Agent AgentBackgroundJob"+s+"\'"
		};
		try{
			Thread.sleep(sleep*1000);
			Process r = Runtime.getRuntime().exec("scp /home/s/smithl4/cs600F2015/project/classes/AgentBackgroundJob"+s+".class aldenv"+mname+":/tmp/Agent");
			r.waitFor();
			//System.out.println("Starting background job "+mname+" "+sleep+" "+kill);
			Process s = Runtime.getRuntime().exec(process);
			s.waitFor();
			//System.out.println("done. "+mname+" "+sleep+" "+kill);
		}catch(Exception e){
		}
	}
}

class TransferJob extends Thread {
	String mname;
	int sleep;
	int kill;
	int counter;

	public TransferJob(String a,int b, int c,int d){
		this.mname=a;
		this.sleep=b;
		this.kill=c;
		this.counter=d;
	}

	public void run() {
		String[] process={
			"/bin/bash",
			"-c",
			"ssh aldenv"+mname+" \'java -cp /tmp Transfer"+counter+"\'"
		};
		try{
			Thread.sleep(sleep*1000);
			Process p = Runtime.getRuntime().exec("scp /home/s/smithl4/cs600F2015/project/classes/Transfer"
					+counter+".class aldenv"+mname+":/tmp");
			p.waitFor();
			//System.out.println("Starting transfer job "+mname+" "+sleep+" "+kill+" "+counter+" ");
			Process q = Runtime.getRuntime().exec(process);
			q.waitFor();
			//System.out.println("done. "+mname+" "+sleep+" "+kill+" "+counter+" ");
		}catch(Exception e){
		}
	}
}
