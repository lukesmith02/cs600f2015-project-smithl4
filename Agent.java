// echo server
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.net.*;
import java.io.*;
import java.sql.Timestamp;


public class Agent {

	//
	//INITIALIZE VARIABLES
	//
	public static int THRESHOLD=25;
	public static int PTIME=10;
	//time before it ignores process
	
	
	public static String[] usedCpu = {
		"/bin/sh",
		"-c",
		"ps -aux | awk -F \' \' \'{ x = x + $3 } END { print x }\'"};
	//bash script used to calculated the CPU
	public static String[] cpuCore = {
		"/bin/sh",
		"-c",
		"cat /proc/cpuinfo | grep cores | head -1"};
	//bash script used to calculated the CPU cores

	static public int machinecount=0; 
	//how many machines are on the network
	public static ArrayList<Double> auction = new ArrayList<Double>();
	//will contain the CPU values for the machines of the network for the auction
	public static Map<Double,String> auctionpair = new HashMap<Double,String>();
	//will contain the machine names and their CPU
	static ArrayList<ClientThread> threads = new ArrayList<ClientThread>();
	static ArrayList<ClientThread> removethreads = new ArrayList<ClientThread>();
	//list of client threads.
	public static int cores=0;


	public static void main(String args[]){
		//
		//INITIALIZE VARIABLES
		//
		BufferedReader input;
		long start_time=0;
		long end_time=0;
		double difference=-1;
		String s = null;
		//String for filereader
		File file=null;
		File file3=null;
		Scanner fscan=null;
		Scanner fscan3=null;
		//file scanner for machines.txt
		String mname=null;
		//machine name variable
		String process="";
		String CMD="";
		Scanner scan = new Scanner(System.in);
		try {
			Process a = Runtime.getRuntime().exec(cpuCore);
			a.waitFor();
			input = new BufferedReader(new InputStreamReader(a.getInputStream()));
			//output of the exec cmd
			process=input.readLine();
			input.close();
			//close the input we started earlier
		}catch(Exception e){
		}

		cores = Integer.parseInt(process.substring(process.length()-1,process.length()));
		try{
			file = new File("machines2.txt");
			fscan = new Scanner(file);
		}
		catch(Exception e){
		}

		startServer();
		//Creates a server socket on port 53421. line 240


		try{
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			mname = localMachine.getHostName();
			System.out.println(mname);
			Thread.sleep(5000);//wait for all server sockets to be open
		}//machine name variable for use in creating sockets
		catch(Exception e){
		}//catch for weird exceptions



		int lukecount=1;
		while ((fscan.hasNext())){
			s = fscan.nextLine();
			if (!s.equals(mname)){
				lukecount++;
			}
			else{
				break;
			}
		}//file scanner that creates client sockets that connect to the other server sockets



		try{
			Thread.sleep(1500*lukecount);//wait for all server sockets to be open
		}catch(Exception e){
		}//catch for weird exceptions


		try{
			file3 = new File("machines2.txt");
			fscan3 = new Scanner(file);
		}
		catch(Exception e){
		}




		//scan.nextLine();

		while ((fscan3.hasNext())){
			s = fscan3.nextLine();
			if (!s.equals(mname)){
				startClient(s+".allegheny.edu",12350);
				System.out.println(s);
				machinecount++;
			}
		}//file scanner that creates client sockets that connect to the other server sockets

		csvCreate(mname);
		System.out.println("Created csv");

		while (true){
			//s=scan.nextLine();
			try {
				Process p = Runtime.getRuntime().exec(usedCpu);
				p.waitFor();
				//get the used CPU of the original machine
				input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				//output of the exec cmd
				process=input.readLine();
				input.close();
				//close the input we started earlier
			}catch(Exception e){
			}
			//output of the exec cmd
			double cpu = Double.parseDouble(process)*2;
			cpu=cpu/cores;
			int ptime= Integer.parseInt(processDetect("time"));
			if ((cpu >= 25) && (ptime>=0) && (ptime<=PTIME))  {
				//is above threshold and processDetect() detects something
				start_time = System.nanoTime();
				int id = Integer.parseInt(processDetect("processid"));
				System.out.println("here");
				try {
					Process x = Runtime.getRuntime().exec("kill -STOP "+id);
					x.waitFor();
				}catch(Exception e){
				}

				String[] proD =processDetect("java").split(" ");
				String dir = proD[0];
				//gets the directory location of the most recent running java process
				String classs = proD[1];
				//gets the name of the most recent running java process including the .class
				String clas = classs.substring(0,classs.length()-6);
				//gets the name of the most recent java program

				//this is removing threads that dont do anything. For example if 4 machines are in the machine.txt and 1 of those is turned off.
				for (ClientThread t : threads){
					//for each of the client threads
					try{
						t.connected().getRemoteSocketAddress().toString().substring(0,9);
					}catch(Exception e){
						removethreads.add(t);
					}
				}
				//this is removing threads that dont do anything. For example if 4 machines are in the machine.txt and 1 of those is turned off.
				for (ClientThread tt : removethreads){
					threads.remove(tt);
				}
				//this is removing threads that dont do anything. For example if 4 machines are in the machine.txt and 1 of those is turned off.
				removethreads = new ArrayList<ClientThread>();






				for (ClientThread t : threads){
					try{
						t.sendMessage(Double.toString(cpu));
						//send a message
					}catch(Exception e){
					}
					//useless catch
				}


				int auctionsize = 0;
				int oldauctionsize = -1;
				while (auctionsize!=oldauctionsize){
					oldauctionsize=auctionsize;
					try{
						Thread.sleep(3000); 
					}catch (Exception e){
					}
					//useless catch
					auctionsize = Agent.auction.size();
				}
				//Wait for all agents to respond


				if (Agent.auction.size() != 0) {

					try {
						Process y = Runtime.getRuntime().exec("kill -9 "+id);
						y.waitFor();
					}catch(Exception e){
					}


					Collections.sort(Agent.auction);
					//sorts the auction list from least to greatest


					System.out.println("Debug winner "+Agent.auction.get(0));
					System.out.println("Debug winner "+ Agent.auctionpair.get(Agent.auction.get(0)));
					//used for debug


					String aldenv = Agent.auctionpair.get(Agent.auction.get(0));
					//get machine name with(the lowest CPU value)
					if (aldenv.charAt(aldenv.length()-1)=='/'){
						aldenv=aldenv.substring(0,aldenv.length()-1);
					}

					String[] redirect = {
						"/bin/bash",
						"-c",
						"ssh "+aldenv+" \'java -cp /tmp/Agent "+clas+" >> /tmp/Agent/out"+clas+".txt\'"
					};
					//bash script for remotely running the java process and redirecting the output to a file
					String[] sshremove = {
						"/bin/bash",
						"-c",
						"ssh "+aldenv+" \'rm /tmp/Agent/out+"+clas+".txt\'"
					};
					//removes the file on the remote machine
					String[] remove = {
						"/bin/bash",
						"-c",
						"rm /tmp/Agent/out"+clas+".txt"
					};
					//removes the file on the local machine
					/*
					   System.out.println(aldenv);
					   System.out.println(dir);
					   System.out.println(classs);
					   System.out.println(clas);
					   */
					try{
						Process o = Runtime.getRuntime().exec(remove);
						o.waitFor();
						//run proccess, then sleep 1 second

						Process p = Runtime.getRuntime().exec(sshremove);
						p.waitFor();
						//run proccess, then sleep 1 second

						Process q = Runtime.getRuntime().exec("scp "+dir+"/"+classs+" "+aldenv+":/tmp/Agent/"+classs);
						q.waitFor();
						//run proccess, then sleep 1 second

						CMD= "ssh "+aldenv+" \'java -cp /tmp/Agent "+clas+" >> /tmp/Agent/out"+clas+".txt\'";

						Process r = Runtime.getRuntime().exec(redirect);
						r.waitFor();
						//run proccess, then sleep 1 second

						Process t = Runtime.getRuntime().exec("scp "+aldenv+":/tmp/Agent/out"+clas+".txt /tmp/Agent");
						t.waitFor();
						//run proccess, then sleep 1 second
						System.out.println("here");
						//Process u = Runtime.getRuntime().exec("cat /tmp/Agent/out"+clas+".txt");
						//u.waitFor();
						//all of these run a command
					}catch (Exception e) {
						System.out.println("crash at process");
					}
					end_time = System.nanoTime();
				}
				else if (Agent.auction.size() == 0) { 
					CMD="no transfer";
					System.out.println("no transfer");
					try {
						Process z = Runtime.getRuntime().exec("kill -CONT "+id);
						z.waitFor();
					}catch(Exception e){
					}
					end_time = System.nanoTime();
				}
				System.out.println("Done");
				difference = (end_time - start_time)/1e9;
				//converts to seconds from nano
				csvFinish(clas,mname,CMD,start_time,end_time,difference);
				auction = new ArrayList<Double>();
				auctionpair = new HashMap<Double,String>();
			}
		}

		//start new auction
	}//end of agent class

	public static void csvCreate(String mname){
		String[] csvCreate = {
			"/bin/bash",
			"-c",
			"echo \"TIMESTAMP,JAVA_NAME,MACHINE_NAME,CMD,THRESHOLD,PTIME,START,END,SECDIFFERENCE\" >> ~/csv_"+mname+".csv"
		};
		try{
			Process u = Runtime.getRuntime().exec(csvCreate);
			u.waitFor();
		}catch(Exception e){
		}
	}


	public static void csvFinish(String javaname, String mname, String CMD, Long start_time, Long end_time, Double difference){
		Date date= new java.util.Date();
		Timestamp TIMESTAMP = new Timestamp(date.getTime());
		String[] csvFinish = {
			"/bin/bash",
			"-c",
			"echo \""+TIMESTAMP+","+javaname+","+mname+","+CMD+","+THRESHOLD+","+PTIME+","+start_time+","+end_time+","+difference+"\" >> ~/csv_"+mname+".csv"
		};
		try{
			Process w = Runtime.getRuntime().exec(csvFinish);
		}catch(Exception e){
		}
	}



	public static String processDetect(String ss){
		String timee="-1";
		String[] processcmd = {
			"/bin/sh",
			"-c",
			"ps -Ao etimes,pid,cmd --sort=etime | grep \"java \""
		};
		String r="-1";
		String processID="-1";
		try {
			String process;
			// getRuntime: Returns the runtime object associated with the current Java application.
			// exec: Executes the specified string command in a separate process.
			Process p = Runtime.getRuntime().exec(processcmd);
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((process = input.readLine()) != null) {
				//System.out.println(process);
				if (!process.contains("/bin/sh")){
					if(!process.contains("grep")){
						if(!process.contains("ssh")){
							if(!process.contains("Agent")){
								if(!process.contains("Process")){
									String[] a = process.split("java ");
									int scount=0;
									String java="java ";
									for (String s : a){
										if (scount==0){
											for (char c : s.toCharArray()){
												if (((c!=' ')) && (timee.equals("-1")))
													timee=c+"";
												else if (!timee.equals("-1"))
													timee=timee+c;
											}
											for (int i=0;i<2;i++){
												if (timee.charAt(timee.length()-1)==' '){
													timee=timee.substring(0,timee.length()-1);
												}
											}
											//System.out.println(timee);
										}
										else
											java=java+s;
										scount++;
									}
									//System.out.println(time);
									//System.out.println(java);
									r=java.split(" ")[2] + " " + java.split(" ")[3] + ".class";

								}
							}
						}
					}
				}
				//System.out.println(process); // <-- Print all Process here line
				// by line	
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		timee=timee.replaceAll("     "," ");
		timee=timee.replaceAll("    "," ");
		timee=timee.replaceAll("   "," ");
		timee=timee.replaceAll("  "," ");
		timee=timee.replaceAll(" "," ");
		if (!timee.contains("-")){
			//System.out.println(timee);
		}
		if (timee.split(" ").length >= 2) {
			processID=timee.split(" ")[1];
			timee=timee.split(" ")[0];
		}
		if (ss.equals("java")){
			return r;
		}
		else if (ss.equals("processid")){
			return processID;
		}
		else if (ss.equals("time")){
			return timee;
		}
		return "error";
	}




	public static void startServer(){
		startServerThread startST = new startServerThread(12350);
		startST.start();
	}

	public static void startClient(String s, int i){
		ClientThread startCT = new ClientThread(s, i);
		startCT.start();
	}

	public static synchronized void addIntoThreadList(ClientThread t){
		Agent.threads.add(t);
	}

	public static synchronized void addIntoList(Double d){
		Agent.auction.add(d);
	}

	public static synchronized void addIntoMap(Double d, String s){
		Agent.auctionpair.put(d,s);
	}


}


class startServerThread extends Thread {

	int socketNum=0;
	Socket socket=null;
	ServerSocket ss=null;

	public startServerThread(int s){
		this.socketNum=s;
	}

	public void run() {
		try {
			ss = new ServerSocket(socketNum);
		}
		catch(IOException e){
			e.printStackTrace();
			System.out.println("Server error");

		}
		System.out.println("Server Listening......");
		while(true){
			try{
				socket = ss.accept();
				System.out.println("Debug Connection Established");
				ServerThread st=new ServerThread(socket);
				st.start();

			}

			catch(Exception e){
				e.printStackTrace();
				System.out.println("Connection Error");

			}
		}


	}	

}


class ClientThread extends Thread {
	int socketNum=0;
	Socket s= new Socket();
	ServerSocket ss=null;
	String host = null;
	PrintWriter s_out = null;
	BufferedReader s_in = null;
	String line=null;


	public ClientThread(String s, int i){
		this.host=s;
		this.socketNum=i;
	}

	public void run() {
		Agent.addIntoThreadList(this);

		try {
			s.connect(new InetSocketAddress(host , socketNum));
			System.out.println("Debug Connected to "+host+" "+socketNum);
			//writer for socket
			s_out = new PrintWriter( s.getOutputStream(), true);
			//reader for socket
			s_in = new BufferedReader(new InputStreamReader(s.getInputStream()));

			line=s_in.readLine();
			String name = this.getName();
			while(line.compareTo("QUIT")!=0){
				//s_out.println("send message");
				//s_out.flush();					
				System.out.println("Debug @server CPU "+name+"> "+line);
				Double winnerd = Double.parseDouble(line);
				Agent.addIntoList(winnerd);
				String winners = s.getRemoteSocketAddress().toString().substring(0,9);
				//System.out.println(winners+" "+winnerd);
				Agent.addIntoMap(winnerd,winners);
				//System.out.println("Debug machine "+s.getRemoteSocketAddress().toString().substring(0,9));
				line=s_in.readLine();
			}
		}
		catch(IOException i)
		{
			System.err.println("Unable to connect to "+host+" "+socketNum);
			//System.exit(1);

		}
		catch(NullPointerException e){
			line=this.getName(); //reused String line for getting thread name
			System.out.println("Client "+line+" Closed");
		}

	}

	public void sendMessage(String s){
		s_out.println( s );
		s_out.flush();
	}

	public Socket connected(){
		return s;
	}

}







class ServerThread extends Thread{  
	public static String[] usedCpu = {
		"/bin/sh",
		"-c",
		"ps -aux | awk -F \' \' \'{ x = x + $3 } END { print x }\'"
	};
	String line=null;
	BufferedReader is = null;
	PrintWriter os=null;
	Socket st=null;

	public ServerThread(Socket s){
		this.st=s;
	}

	public void run() {
		try{
			is= new BufferedReader(new InputStreamReader(st.getInputStream()));
			os= new PrintWriter(st.getOutputStream());


		}catch(IOException e){
			System.out.println("IO error in server thread");
		}

		try {
			line=is.readLine();	
			String name = this.getName();
			while(line.compareTo("QUIT")!=0){
				System.out.println("Debug @client CPU "+name+"> "+line);
				try {
					String process;
					Process p = Runtime.getRuntime().exec(usedCpu);
					p.waitFor();
					BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
					while ((process = input.readLine()) != null) {
						double temp = Double.parseDouble(process);
						temp=temp/Agent.cores;
						process = Double.toString(temp);
						if (Double.parseDouble(line)>Double.parseDouble(process)){
							os.println(process);
							os.flush();
						}
					}
					input.close();
				}catch (Exception err) {
					err.printStackTrace();
				}
				line=is.readLine();

			}
		} catch (IOException e) {

			line=this.getName(); //reused String line for getting thread name
			System.out.println("IO Error/ Client "+line+" terminated abruptly");
		}
		catch(NullPointerException e){
			line=this.getName(); //reused String line for getting thread name
			System.out.println("Client "+line+" Closed");
		}

		finally{
			try{
				System.out.println("Connection Closing..");
				if (is!=null){
					is.close(); 
					System.out.println(" Socket Input Stream Closed");
				}

				if(os!=null){
					os.close();
					System.out.println("Socket Out Closed");
				}
				if (st!=null){
					st.close();
					System.out.println("Socket Closed");
				}

			}
			catch(IOException ie){
				System.out.println("Socket Close Error");
			}
		}//end finally
	}
}
